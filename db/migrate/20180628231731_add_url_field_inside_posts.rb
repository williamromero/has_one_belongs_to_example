class AddUrlFieldInsidePosts < ActiveRecord::Migration[5.2]
  def change
    add_column :posts, :url_image, :text, after: :image_two
  end
end
