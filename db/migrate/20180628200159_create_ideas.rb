class CreateIdeas < ActiveRecord::Migration[5.2]
  def change
    create_table :ideas do |t|
      t.references :post, foreign_key: true
      t.text :content

      t.timestamps
    end
  end
end
