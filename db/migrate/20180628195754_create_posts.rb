class CreatePosts < ActiveRecord::Migration[5.2]
  def change
    create_table :posts do |t|
      t.text :description
      t.text :image_one
      t.text :image_two

      t.timestamps
    end
  end
end
